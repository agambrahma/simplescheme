import fastparse._
import SingleLineWhitespace._

/*
data LispVal = Atom String
             | List [LispVal]
             | DottedList [LispVal] LispVal
             | Number Integer
             | String String
             | Bool Bool
*/
sealed trait SchemeVal
case class SchemeAtom(name: String) extends SchemeVal
case class SchemeList(list: List[SchemeVal]) extends SchemeVal
case class SchemeDottedList(list: List[SchemeVal], tailElem: SchemeVal) extends SchemeVal
case class SchemeNumber(number: Int) extends SchemeVal
case class SchemeString(str: String) extends SchemeVal
case class SchemeBoolean(bool: Boolean) extends SchemeVal

object Scheme {
  val ParseSuccess = "Found a value"

  def stringParser[_: P] = P( "\"" ~ CharsWhile(_ != '\"').! ~ "\"" ).map( SchemeString(_) )

  def atomParser[_: P] = P( CharIn("a-zA-Z!#$%&|*+-/:<=>?@^_~") ~ CharIn("0-9a-zA-Z!#$%&|*+-/:<=>?@^_~").rep ).!.map( SchemeAtom(_) )

  def numberParser[_: P] = P( CharIn("0-9").rep.! ).map( (x) => SchemeNumber(x.toInt) )

  def exprParser[_: P]: P[SchemeVal] = P( atomParser | stringParser | numberParser | "(" ~ listParser ~ ")" )

  def listParser[_: P]: P[SchemeList] = P( exprParser.!.rep(sep=" ") ).flatMap( SchemeList(_) )

  def process(line: String): String = {
    parse(line, exprParser(_)) match {
      case Parsed.Success(result, _) => {
        println(s"Debug: [$result]\n")
        ParseSuccess
      }
      case Parsed.Failure(error) => s"Invalid expression: $error"
    }
  }
}

