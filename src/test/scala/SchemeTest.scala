import Scheme._
import org.scalatest.funsuite.AnyFunSuite

class SchemeTest extends AnyFunSuite {
  test("numbers") {
    val input = List("12", "25", "43")
    val output = input

    for ((i,o) <- input.zip(output)) {
      assert(Scheme.process(i) == Scheme.ParseSuccess)
    }
  }

  test("atoms") {
    val input = List("abc", "f%$#^", "h567896", "28iakdsjh293@#%$&", "@$#$gfd324")
    val output = input

    for ((i, o) <- input.zip(output)) {
      assert(Scheme.process(i) == Scheme.ParseSuccess)
    }
  }

  test("strings") {
    val input = List("\"here is a string\"", "\"... And here is another\"")
    val output = input

    for ((i, o) <- input.zip(output)) {
      assert(Scheme.process(i) == Scheme.ParseSuccess)
    }
  }

  test("lists") {
    val input = List("(a ball)", "(24 12)", "(foo 12)")
    val output = input

    for ((i, o) <- input.zip(output)) {
      assert(Scheme.process(i) == Scheme.ParseSuccess)
    }
  }
}
